import os
import pandas as pd

def list_subdirectories(folder_path):
    subdirectories = [f.path for f in os.scandir(folder) if f.is_dir()]
    return subdirectories

def merge_csv_in_folders(folder_path, runNumber, layer, fileName):
    if (layer == "ALL"):
        output_dir = "./results/"+runNumber+"/"
    else:
        output_dir = "./results/"+runNumber+"/" + layer
    os.makedirs(output_dir, exist_ok=True)

    if (layer == "ALL"):
        subfolders = [f.path for f in os.scandir(folder_path+runNumber+"/") if f.is_dir()]
    else:
        subfolders = [f.path for f in os.scandir(os.path.join(folder_path+runNumber+"/", layer)) if f.is_dir()]

    combined_data = pd.DataFrame()
    
    for subfolder in subfolders:
        csv_file_path = os.path.join(subfolder, fileName)
        print(csv_file_path)
      
        if os.path.isfile(csv_file_path):
            try:
                df = pd.read_csv(csv_file_path, header=None, names=['column1', 'column2', 'column3'])
                #df = pd.read_csv(csv_file_path, header=None, names=['column1', 'column2'])
                combined_data = pd.concat([combined_data, df], ignore_index=True, axis=0)
            except pd.errors.EmptyDataError:
                print(f"Warning: Empty CSV file found in {subfolder}. Skipping.")
            except pd.errors.ParserError:
                print(f"Error: Unable to parse CSV file in {subfolder}. Skipping.")
        else:
            print(f"Warning: 'fitChi2.csv' not found in {subfolder}. Skipping.")

    output_file_path = os.path.join(output_dir, fileName)
    combined_data.to_csv(output_file_path, header=False, index=False)
    print(f"Merged CSV files saved to: {output_file_path}")

def mergeACSide(folder_path, runNumber1, runNumber2, fileNames):
    for fileName in fileNames:
        output_folder = "./results/"+runNumber1 + "_" + runNumber2+"/"
        os.makedirs(output_folder, exist_ok=True)

        input1 = folder_path + runNumber1 + "/"
        input2 = folder_path + runNumber2 + "/"

        csv_file_path1 = os.path.join(input1, fileName)
        csv_file_path2 = os.path.join(input2, fileName)

        df1 = pd.read_csv(csv_file_path1, header=None, names=['column1', 'column2', 'column3'], low_memory=False)
        df2 = pd.read_csv(csv_file_path2, header=None, names=['column1', 'column2', 'column3'], low_memory=False)

        output_file = os.path.join(output_folder, fileName)
        if os.path.exists(output_file):
            os.remove(output_file)

        merged_df = pd.concat([df1, df2], ignore_index=True)
        merged_df.to_csv(output_file, header=False, index=False)
    
    
def mergeFile(folder_path, runNumber, layer, fileName):
    for l in layer:
        for f in fileName:
            merge_csv_in_folders(folder_path, runNumber, l, f)
    

runNumber1 = "0000285706"

folder_path = "./results/"
fileName = ['pulse.csv']
layer = ["UTaX", "UTaU", "UTbV", "UTbX", "ALL"]

mergeFile(folder_path, runNumber1, layer, fileName)
