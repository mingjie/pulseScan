import torch
import torch.nn as nn
import torch.nn.functional as F
import pytorch_lightning as pl
from torch import Tensor


class Simple_MLP(pl.LightningModule):
    def __init__(self, input_dim, units):
        super(Simple_MLP, self).__init__()   #inheritance from base class - In Python 3, the super(Square, self) call is equivalent to the parameterless super() call.
        
        self.input_dim = input_dim
        self.units = units
        self.layers = nn.Sequential(
            nn.Linear(input_dim, units),#.to(device) - i don't think it's necessary as The Trainer will run on all available GPUs by default. Make sure you’re running on a machine with at least one GPU. There’s no need to specify any NVIDIA flags as Lightning will do it for you.
            nn.Tanh(),
            nn.Linear(units, 1, bias=False),#.to(device)
            nn.Tanh()
        )
        
        self.mse = nn.MSELoss(reduction='mean')
        
        
    def forward(self, x):
        return self.layers(x)
        
    def training_step(self, batch, batch_idx):
        x, y = batch
        x = x.view(x.size(0), -1)
        y_hat = self.layers(x)
        loss = self.mse(y_hat, y)
        self.log('train_loss', loss)
        return loss

    # def test_step(self, bacth, batch_idx):
        
    
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters())
        return optimizer