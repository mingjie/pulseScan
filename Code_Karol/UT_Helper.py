import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pickle
import ROOT

def draw_UT(tab, plane, fig, ax):
    im = ax.imshow(tab, origin="lower", extent=(0,36,0,28), aspect = 'auto')

    # ax = plt.gca()
    ax.set_xticks(np.arange(0, 36, 2))
    ax.set_yticks(np.arange(0, 28, 2))
    ax.set_xticklabels([])
    ax.set_yticklabels([])

    ys = ["M4B", "S3B", "M3B", "S2B", "M2B", "S1B", "M1B", "M1T", "S1T", "M2T", "S2T", "M3T", "S3T", "M4T"]
    staves = ['S9', 'S8', 'S7', 'S6', 'S5', 'S4', 'S3', 'S2', 'S1', 'S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9']

    ax.set_xticks(np.arange(1, 37, 2), minor=True)
    ax.set_xticklabels(staves, minor=True)
    ax.set_yticks(np.arange(1, 29, 2), minor=True)
    ax.set_yticklabels(ys, minor=True)

    # Gridlines based on minor ticks
    ax.grid(color='gray', linestyle='-', linewidth=1)
    ax.plot([15,15], [10,18], ls = '-', color = 'gray', lw=1)
    ax.plot([17,17], [10,18], ls = '-', color = 'gray', lw=1)
    ax.plot([19,19], [10,18], ls = '-', color = 'gray', lw=1)
    ax.plot([21,21], [10,18], ls = '-', color = 'gray', lw=1)
    ax.plot([16,20], [13,13], ls = '-', color = 'gray', lw=1)
    ax.plot([16,20], [15,15], ls = '-', color = 'gray', lw=1)

    ax2 = ax.twiny()
    ax2.set_xticks(np.arange(0, 37, 18))
    ax2.set_xticklabels([])
    ax2.grid(color='gray', linestyle='-', linewidth=3)
    ax2.tick_params(axis='x', colors='white') 

    ax3 = ax.twinx()
    ax3.set_yticks(np.arange(0, 29, 14))
    ax3.set_yticklabels([])
    ax3.grid(color='gray', linestyle='-', linewidth=3)
    ax3.tick_params(axis='y', colors='white') 

    ax3.plot(18,14, 'wo', ms = 10, mec = 'gray', mew = '1')

    cbar = plt.colorbar(im)


    ax.set_title(plane)


    if (plane == "UTaX") or (plane == "UTaU"):
        ax.set_xlim([2,34])
        ax2.set_xlim([2,34])
        ax3.set_xlim([2,34])
        
    if plane not in ["UTaX", "UTbX", "UTaU", "UTbV"]:
        raise ValueError("Wrong UT plane name!")
    
    return [fig, ax, im]

def map_UT_counts(paths, UT_map):
    UTaX = np.zeros((28,36))
    UTaU = np.zeros((28,36))
    UTbV = np.zeros((28,36))
    UTbX = np.zeros((28,36))

    counts = pd.Series([path.split("/")[4] for path in paths]).value_counts()
    for i in range(len(counts)):
        plane = counts.index[i][:4]
        map_vals = UT_map[counts.index[i][5:]]
        for val in map_vals:
            if plane == "UTaX":
                UTaX[val] += counts.iloc[i]
            if plane == "UTaU":
                UTaU[val] += counts.iloc[i]
            if plane == "UTbV":
                UTbV[val] += counts.iloc[i]
            if plane == "UTbX":
                UTbX[val] += counts.iloc[i]
                
    return [UTaX, UTaU, UTbV, UTbX]

def create_hdict(paths):
    hdict = {}
    for filename in paths:
        hdict[filename] = {
            "hist_tab": [],
        }
        f = ROOT.TFile(filename)
        mh = ROOT.TH1F("mean hist", ";Time - offset [ns]; Signal Height [LSB]", 256, 0, 100)
        for i in range(128):
            hist = f.Get("hPs_" + f'{i:03d}')
            hdict[filename]["hist_tab"].append(hist)
            for nb in range(1,257):
                mh.AddBinContent(nb, hist.GetBinContent(nb))
        mh.Scale(1./128)
        mh.SetOption("hist")
        hdict[filename]["mean_hist"] = mh
        f.Clear()

    total_mh = ROOT.TH1F("total mean hist", ";Time - offset [ns]; Signal Height [LSB]", 256, 0, 100)
    for filename in paths:
        mh = hdict[filename]["mean_hist"]
        for nb in range(1,257):
            total_mh.AddBinContent(nb, mh.GetBinContent(nb))
        f.Clear()
    total_mh.Scale(1./len(paths))
    total_mh.SetOption("hist")
    hdict["total_mean_hist"] = total_mh
    
    return hdict

class HQ_DATA():
    def __init__(self, paths, hdict, name = ""):
        self.HQ_hist = ROOT.TH1F(name + "HQ hist", "; Time around maximum [ns]; Signal Height [LSB]", 768, -100, 200)
        self.max_loc_tab = []
        self.max_tab = []
        self.width_tab = []
        

        
        
        ctr = 0
        for path in paths:
            for hist in hdict[path]["hist_tab"]:
                
                max_bin = hist.GetMaximumBin()
                max_val = hist.GetBinContent(max_bin)
                sigma_hist = ROOT.TH1F("sigma hist", "sigma hist", 768, -100, 200)
                if max_val > 8:
                    max_loc = hist.GetBinCenter(max_bin)
                    earlier_val = hist.GetBinContent(hist.FindBin(max_loc-25))
                    later_val = hist.GetBinContent(hist.FindBin(max_loc+25))
                    if (earlier_val/max_val < 0.04) and (later_val/max_val < 0.04):
                        self.max_tab.append(max_val)
                        self.max_loc_tab.append(max_loc)
                        
                        for i in range(1,hist.GetNbinsX()+1):
                            xval = hist.GetBinCenter(i) - max_loc
                            yval = hist.GetBinContent(i)
                            HQ_bin = self.HQ_hist.FindBin(xval) 
                            self.HQ_hist.AddBinContent(HQ_bin, yval)
                            
                            if abs(xval) < 20:
                                sigma_hist.SetBinContent(HQ_bin, yval)

                        
                        y_min = sigma_hist.GetMinimum()
                        for i in range(1,hist.GetNbinsX()+1):
                            xval = hist.GetBinCenter(i) - max_loc
                            yval = hist.GetBinContent(i)
                            HQ_bin = self.HQ_hist.FindBin(xval) 
                            
                            if abs(xval) < 20:
                                sigma_hist.SetBinContent(HQ_bin, yval-y_min)
                        width = 2.35*sigma_hist.GetStdDev()
                        self.width_tab.append(width)
                        ctr+=1
                        
        self.HQ_hist.Scale(1/ctr)
        self.xmax_trim = 100 - np.min(self.max_loc_tab)
        self.xmin_trim = -np.max(self.max_loc_tab)
        
        self.x_train = []
        self.y_train = []
        for i in range(1,self.HQ_hist.GetNbinsX()+1):
            x = self.HQ_hist.GetBinCenter(i)
            y = self.HQ_hist.GetBinContent(i)
            
            if x >= self.xmin_trim and x <= self.xmax_trim:
                self.x_train.append(x)
                self.y_train.append(y)
                
def plot_1d_function(x_train, y_train, x_eval, predictions, units, epochs):
    fig, ax = plt.subplots()
    ax.axvspan(x_train.flatten()[0], x_train.flatten()[-1], alpha=0.15, color='limegreen')
    ax.plot(x_eval, predictions, '-', color='darkorange', linewidth=2.0, label = 'MLP output g(x)')
    ax.plot(x_train, y_train, '.', color='royalblue', label = 'Training set')
    ax.grid(which='both');
    # ax.rcParams.update({'font.size': 14})
    ax.set_xlabel('Time around maximum [ns]');
    ax.set_ylabel('Signal width [LSB]')
    ax.set_title('%d neurons in hidden layer with %d epochs of training' % (units ,epochs))
    ax.legend()
    return fig, ax
