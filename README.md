### Mingjie's code:
***plot_pulse.C*** -  used to analyze the data. In this code, I also store the pulse peak for each channel in a CSV file.

***mergeCSV.py*** - used to merge all the CSV file
### Karol's code:
***PulseShapes.ipynb*** - notebook responsible for reading the data and basic analysis

***PS_network.ipynb*** - implementation of MLP network learning the impulse shape

***UT_helper.py*** - just a helper file with some definitions useful e.g. for drawing

***UT_map.pkl*** - pickle file storing the mapping used for drawing the UT planes